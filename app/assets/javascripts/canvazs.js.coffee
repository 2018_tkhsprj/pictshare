
$ ->
    canvas = $("#draw-area")
    ctx = canvas[0].getContext('2d')
    ctx.lineWidth = 1
    
    ctx.putPoint = (x, y) ->
        @.beginPath()
        @.arc(x, y, @.lineWidth / 2.0, 0, Math.PI*2, false)
        @.fill()
        @.closePath()
    
    ctx.drawLine = (sx, sy, ex, ey) ->
        @.beginPath()
        @.moveTo(sx, sy)
        @.lineTo(ex, ey)
        @.stroke()
        @.closePath()
    
    mousedown = false
    canvas.mousedown (e) ->
        #alert("Hello #{e.pageX}and#{e.pageY}!")
        ctx.savePrevData()
        ctx.prevPos = e
        ctx.target = $(e.target)
        mousedown = true
        ctx.putPoint(ctx.prevPos.pageX - ctx.target.position().left, ctx.prevPos.pageY - ctx.target.position().top)
    
    canvas.mousemove (e) ->
        return unless mousedown
        nowPos = e
        ctx.target = $(e.target)
        ctx.drawLine(ctx.prevPos.pageX - ctx.target.position().left, ctx.prevPos.pageY - ctx.target.position().top, nowPos.pageX - ctx.target.position().left, nowPos.pageY - ctx.target.position().top)
        ctx.putPoint(nowPos.pageX - ctx.target.position().left, nowPos.pageY - ctx.target.position().top)
        ctx.prevPos = nowPos
    
    canvas.mouseup (e) ->
        mousedown = false
    canvas.mouseout (e) ->
        mousedown = false
    
    $("#clear-button").click ->
        ctx.clearRect(0, 0, canvas.width(), canvas.height())
    
    ctx.savePrevData = ->
        @.prevImageData = @.getImageData(0, 0, canvas.width(), canvas.height())
    
    $("#return-button").click ->
        ctx.putImageData(ctx.prevImageData, 0, 0)
        
    $("#pen-width-slider").change ->
        ctx.lineWidth = $(@).val()
        $("#show-pen-width").text(ctx.lineWidth)
    
    ctx.setColor = ->
        color = "rgb(#{red_slider.val()},#{green_slider.val()},#{blue_slider.val()})"
        @.strokeStyle = color
        @.fillStyle = color
        preview_color.css('background-color', color)
    
    red_slider = $("#pen-color-red-slider")
    green_slider = $("#pen-color-green-slider")
    blue_slider = $("#pen-color-blue-slider")
    preview_color = $("#preview-color")
    
    red_slider.change ->
        ctx.setColor()
        $("#show-pen-red").text($(@).val())
    green_slider.change ->
        ctx.setColor()
        $("#show-pen-green").text($(@).val())
    blue_slider.change ->
        ctx.setColor()
        $("#show-pen-blue").text($(@).val())
        
    $("#save-button").click ->
        _form = $("#new_dfpic")
        canvas = canvas[0].toDataURL()
        $("#dfpic_dfimage").val(canvas)
        _form.submit()
        #$.post '/pictures', {data: url}
        
    
###
  $("div#canvas").dblclick (e) ->
    canvas = $(e.target)
    x = e.pageX - canvas.position().left
    y = e.pageY - canvas.position().top
    canvas.append("<div class='block' style='left: #{x}px; top: #{y}px;' />")
###