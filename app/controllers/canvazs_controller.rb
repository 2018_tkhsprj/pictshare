class CanvazsController < ApplicationController
  include CarrierwaveBase64Uploader
  
  def new
    @dfpic = Dfpic.new
    #render 'new'
  end
  
  def create
    tmp_dfpic_params = dfpic_params
    image_data = base64_conversion(tmp_dfpic_params[:dfimage])
    ###TestPriv###
       user = User.find_by(name: session[:login_name])
       title = "test"
       date = Time.new
    ##############
    dfpic = Dfpic.new(date: date, dfimage: image_data, title: title)
    user.dfpics << dfpic
    df = Dfpic.last
    tag = Tag.new(tagname: "原案")
    df.tags << tag
    redirect_to '/'
  end
  
  private
    def dfpic_params
      params.require(:dfpic).permit(:date, :user_id, :dfimage, :title)
    end
    
end
