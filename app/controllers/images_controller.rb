class ImagesController < ApplicationController
    
    def show
        @supic = Supic.new
        @dfpic = Dfpic.find(params[:id])
        render 'mimage'
    end
    
    def create
        user = User.find_by(name: session[:login_name])
        d = Dfpic.find(params[:supic][:id])
        suimage = params[:supic][:suimage]
        s = Supic.new(date: Time.new, title: Takarabako.open, suimage: suimage)
        user.supics << s
        s = Supic.last
        d.supics << s
        redirect_to root_path
    end
    
    def supic
        @user = User.find_by(name: session[:login_name])
        @dfpic = Dfpic.find(params[:id])
    end

end
