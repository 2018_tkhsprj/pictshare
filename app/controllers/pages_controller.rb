class PagesController < ApplicationController
    PER = 8
    
    def index
        #@dfpics = Dfpic.paginate(page: params[:page])
        @user = User.find_by(name: session[:login_name])
        @search = Dfpic.ransack(params[:q])
        @result = @search.result(distinct: true).page(params[:page]).per(9)
    end
    
    def dfsearch
        @q = "原案"
        @user = User.find_by(name: session[:login_name])
        @search = Dfpic.ransack(params[:q])
        user = User.find_by(name: session[:login_name])
        dfpics = user.dfpics
        @result = dfpics.page(params[:page]).per(3)
        render 'index'
    end
    
    def susearch
        @q = "提案"
        @user = User.find_by(name: session[:login_name])
        @search = Supic.ransack(params[:q])
        user = User.find_by(name: session[:login_name])
        supics = user.supics
        @result = supics.page(params[:page]).per(3)
        render 'index'
    end
    
    def folsearch
        @user = User.find(params[:id])
        @dfresult = @user.dfpics.page(params[:page]).per(3)
        @suresult = @user.supics.page(params[:page]).per(3)
    end
    
end
