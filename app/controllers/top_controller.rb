class TopController < ApplicationController
  def home
  	@user = User.find_by(name: session[:login_name])
  	@q = Dfpic.ransack(params[:q])
  end
  
  def login
    user = User.authenticate(params[:name], params[:password_digest])
		if user
			session[:login_name] = params[:name]
			redirect_to root_path
		else
			#flash[:alert] = '現在ログインしていません'
			render 'top/login'
		end
  end
  
  def logout
		session.delete(:login_name)
		redirect_to root_path
  end
end
