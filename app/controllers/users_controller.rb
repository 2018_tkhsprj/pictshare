class UsersController < ApplicationController
  def update
    user = session[:login_name]
    @user = User.find_by(name: user)
    name = params[:user][:name]
    mail = params[:user][:mail]
    intro = params[:user][:intro]
    @user.update(name: name, mail: mail, intro: intro)
    @user.save
    ###carrierwaveエラー対策###
    userimage = params[:user][:userimage]
    if userimage != @user.userimage
      @user.update(userimage: userimage)
      @user.save
    else
      redirect_to user_path
    end
    redirect_to user_path
  end
    
  def new
      @user = User.new
  end
  def create
      @user = User.new(
          name: params[:user][:name],
          mail: params[:user][:mail],
          age: params[:user][:age],
          password_digest: params[:user][:password_digest]
          )
      if @user.save
          redirect_to top_login_path
      else
          render 'user/new'
      end
  end
  
  def show
    if session[:login_name]
      user = session[:login_name]
      @user = User.find_by(name: user)
      render 'profile'
    else
      redirect_to '/'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end

    def follow
      you = User.find_by(name: session[:login_name])
      d = params[:id]
      oth_user = Dfpic.find(d).user
      you.follow(oth_user)
      redirect_to '/'
    end
    
    def unfollow
      you = User.find_by(name: session[:login_name])
      d = params[:id]
      oth_user = Dfpic.find(d).user
      you.unfollow(oth_user)
      redirect_to '/'
    end
    
    def folfow
      id = params[:id]
      fol = params[:fol]
      if fol == "follow"
        @user = User.find(id).following
      elsif fol == "follower"
        @user = User.find(id).followers
      else
        @user = nil
      end
      @fol = params[:fol]
    end
  
end
