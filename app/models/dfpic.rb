class Dfpic < ActiveRecord::Base
    mount_uploader :dfimage, ImagesUploader
    
    belongs_to :user
    has_many :tags, dependent: :destroy
    has_many :supics, dependent: :destroy
    has_many :goodjobs, dependent: :destroy
    has_many :gj_users, source: :user, through: :goodjobs
end