class Supic < ActiveRecord::Base
    mount_uploader :suimage, ImagesUploader
    
    belongs_to :user
    belongs_to :dfpic
    has_many :comments, dependent: :destroy
    has_many :goodjobs, dependent: :destroy
    has_many :gj_users, source: :user, through: :goodjobs
end