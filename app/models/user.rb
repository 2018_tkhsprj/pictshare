class User < ActiveRecord::Base
    mount_uploader :userimage, ImagesUploader
    
    has_many :goodjobs, dependent: :destroy
    has_many :comments, dependent: :destroy
    has_many :dfpics, dependent: :destroy
    has_many :gj_dfpics, source: :dfpic, through: :goodjobs, dependent: :destroy
    has_many :supics, dependent: :destroy
    has_many :gj_supics, source: :supic, through: :goodjobs, dependent: :destroy
    
    ########################################################################
    # フォロー機能関連付け                                                 #
    ########################################################################
    # 能動的関係の関連付け                                                 #
    has_many :active_relationships, class_name:  "Relationship",           # 
                                    foreign_key: "follower_id",            #
                                    dependent: :destroy                    #
    # followingの関連付け                                                  #
    has_many :following, through: :active_relationships, source: :followed #
    ########################################################################
    
    ########################################################################
    # フォロワー機能関連付け                                               #
    ########################################################################
    # 受動的関係の関連付け                                                 #
    has_many :passive_relationships, class_name:  "Relationship",          #
                                   foreign_key: "followed_id",             #
                                   dependent:   :destroy                   #
    # followedの関連付け                                                   #
    has_many :followers, through: :passive_relationships, source: :follower#
    ########################################################################
    
    ########################################################################
    # 関連付け用呼び出しメソッド                                           #
    ########################################################################
    # フォローする                                                         #
    def follow(other_user)                                                 #
      active_relationships.create(followed_id: other_user.id)              #
    end                                                                    #
                                                                           #
    #アンフォローする                                                      #
    def unfollow(other_user)                                               #
      active_relationships.find_by(followed_id: other_user.id).destroy     #
    end                                                                    #
                                                                           #
    #フォロー判定                                                          #
    def following?(other_user)                                             #
      following.include?(other_user)                                       #
    end                                                                    #
    ########################################################################
    
    validates :name , uniqueness: true
    
    def self.authenticate(name,password_digest)
          user = User.find_by(name: name)
        if user && user.password_digest == password_digest
          user
        else
          nil
        end
    end
    
end