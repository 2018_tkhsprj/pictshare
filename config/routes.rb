Rails.application.routes.draw do
  root 'top#home'
  get 'canvazs/new', to: 'canvazs#new'
  get 'users/upload', to: 'users#upload'
  get 'users/follow', to: 'users#follow'
  get 'users/unfollow', to: 'users#unfollow'
  get 'users/folfow', to: 'users#folfow'
  get 'pages/dfsearch', to: 'pages#dfsearch'
  get 'pages/susearch', to: 'pages#susearch'
  get 'pages/folsearch', to: 'pages#folsearch'
  get 'images/supic', to: 'images#supic'
  get 'top/login'
  post 'top/login'
  get 'top/logout'
  resources:users
  resources:pages
  resources:images
  resources:uploads
  resources :canvazs, only: [:create]
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
