class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :mail
      t.integer :age
      t.string :password_digest
      t.string :userimage
      t.string :intro

      t.timestamps null: false
    end
  end
end
