class CreateDfpics < ActiveRecord::Migration
  def change
    create_table :dfpics do |t|
      t.date :date
      t.integer :user_id
      t.string :dfimage
      t.string :title

      t.timestamps null: false
    end
  end
end
