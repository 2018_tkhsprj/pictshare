class CreateSupics < ActiveRecord::Migration
  def change
    create_table :supics do |t|
      t.date :date
      t.integer :user_id
      t.string :title
      t.string :suimage
      t.integer :dfpic_id

      t.timestamps null: false
    end
  end
end
