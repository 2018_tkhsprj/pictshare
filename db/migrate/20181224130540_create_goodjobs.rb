class CreateGoodjobs < ActiveRecord::Migration
  def change
    create_table :goodjobs do |t|
      t.integer :user_id
      t.integer :dfpic_id
      t.integer :supic_id

      t.timestamps null: false
    end
  end
end
