class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :followed_id
      t.integer :follower_id

      t.timestamps null: false
    end
    
    ######################################################################
    # 複合キーインデックス,ユニーク宣言(重複登録対策)                    #
    ######################################################################
    add_index :relationships, :follower_id                               #
    add_index :relationships, :followed_id                               #
    add_index :relationships, [:follower_id, :followed_id], unique: true #
    ######################################################################
    
  end
end
