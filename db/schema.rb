# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181224133936) do

  create_table "comments", force: :cascade do |t|
    t.string   "message"
    t.integer  "supic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "dfpics", force: :cascade do |t|
    t.date     "date"
    t.integer  "user_id"
    t.string   "dfimage"
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "goodjobs", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "dfpic_id"
    t.integer  "supic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "relationships", force: :cascade do |t|
    t.integer  "followed_id"
    t.integer  "follower_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "supics", force: :cascade do |t|
    t.date     "date"
    t.integer  "user_id"
    t.string   "title"
    t.string   "suimage"
    t.integer  "dfpic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string   "tagname"
    t.integer  "dfpic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "mail"
    t.integer  "age"
    t.string   "password_digest"
    t.string   "userimage"
    t.string   "intro"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

end
